# List Widget Alter

This module creates a customizable Field Widget for text, numbers, and float lists.
It allows the admin to alter the field options by providing a separate label for each one, or hide it all together.


## Features
Sometimes the list fields are reused in different places from a site, and the labels of the options (field definition level) don't always make sense everywhere the field is used.
This widget allows the administrators to present the options differently in a form display level.

- You can opt to show/hide options
- You can change the label the end user sees for each option
- The module supports select lists, checkboxes and radio buttons

When an option is disabled, it will be removed from the form containing the field. If there was previous content with the removed option, the option will still be available until changed, but a notice message will be added to the label. This text is also customizable, at the modules configuration page.


## Post-Installation
Go to your entity's form display and select the Widget "Altered list" for your list field. Then click on the Configuration icon, and set the desired configuration.

Choose how the field should be displayed, if as a select list or checkboxes/radio buttons (depends on the cardinality of your field), and mark all available options. You can alter the labels in the specified fields.

Save the changes and the form.


## Additional Requirements
No aditional requirements.


## Recommended modules/libraries
No recommended modules/libraries.


## Similar projects
None that I could find.


## Supporting this Module
If you find an issue, report it. If you can help solve one, you're welcome to.
This module is under active development.


## Community Documentation
None yet.


## Maintainers
[anacolautti](https://www.drupal.org/u/anacolautti)
