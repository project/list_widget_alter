<?php

namespace Drupal\list_widget_alter\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'list_widget_alter_select' widget.
 *
 * Limits the list_string defined in the field at a widget level. This way
 * we get to use this field in different places, and limit the users to the
 * options that make sense in each case.
 *
 * @FieldWidget(
 *   id = "list_widget_alter_list",
 *   label = @Translation("Altered list"),
 *   field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string"
 *   },
 *   multiple_values = TRUE
 * )
 */
class ListWidgetAlterList extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'widget_type' => 'select',
      'choices' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $base_options = [];

    $settings = $this->fieldDefinition->getSetting('allowed_values');
    foreach ($settings as $key => $value) {
      $base_options[$key] = [
        'label' => $value,
        'is_enabled' => TRUE,
      ];
    }

    $element['widget_type'] = [
      '#type' => 'select',
      '#title' => t("Widget type"),
      '#options' => [
        'select' => $this->t("Select list"),
        'choice' => $this->t("Checkboxes/Radio buttons"),
      ],
      '#default_value' => $this->getSetting('widget_type'),
    ];

    $altered_options = $this->getSetting('choices');
    $element['choices'] = [
      '#type' => 'fieldset',
      '#title' => t('Displayed options'),
      '#description' => $this->t("Disabling an option does not remove it from previously created content. A deprecation notice (you can change the message at the module's configuration page) is added to the label of the option, and once manually removed in each content, is hidden from the user."),
    ];

    foreach ($base_options as $key => $option) {
      $element['choices'][$key] = [
        '#type' => 'fieldset',
        '#title' => t("@option", ["@option" => $option['label']]),
      ];
      $element['choices'][$key]['is_enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t("Enable this option"),
        '#default_value' => array_key_exists($key, $altered_options) ? $altered_options[$key]['is_enabled'] : $option['is_enabled'],
      ];
      $element['choices'][$key]['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t("Label"),
        '#default_value' => array_key_exists($key, $altered_options) ? $altered_options[$key]['label'] : $option['label'],
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $choices = $this->getSetting('choices');
    $options = [];
    foreach ($choices as $option) {
      if ($option['is_enabled']) {
        $options[] = $option['label'];
      }
    }
    $summary[] = $this->t("Display as @widget_type.", ['@widget_type' => $this->getSetting('widget_type')]);
    if (!empty($options)) {
      $summary[] = $this->t(
        'Options: @options', [
          '@options' => implode(", ", array_values($options)),
        ]
      );
    }
    else {
      // @todo test
      $summary[] = $this->t('Using default settings.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $choices = $this->getSetting('choices');
    $options = $element['#options'];
    $new_options = [];
    foreach ($options as $key => $option) {
      if (array_key_exists($key, $choices)) {
        if ($choices[$key]['is_enabled']) {
          $new_options[$key] = $choices[$key]['label'];
        }
      }
      else {
        $new_options[$key] = $option;
      }
      if (in_array($key, $element['#default_value']) && !array_key_exists($key, $new_options)) {
        if (array_key_exists($key, $choices)) {
          $new_options[$key] = $choices[$key]['label'] . " - " . \Drupal::config('list_widget_alter.settings')->get('removed_option_message');
        }
        else {
          $new_options[$key] = $option . " - " . \Drupal::config('list_widget_alter.settings')->get('removed_option_message');
        }

      }
    }
    $element['#options'] = $new_options;

    if ($this->getSetting('widget_type') == 'select') {
      $element['#type'] = 'select';
    }
    else {
      if ($this->fieldDefinition->getFieldStorageDefinition()->get('cardinality') == 1) {
        $element['#type'] = 'radios';
      }
      else {
        $element['#type'] = 'checkboxes';
      }
    }

    return $element;
  }

}
