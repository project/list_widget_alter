<?php

namespace Drupal\list_widget_alter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * {@inheritdoc}
 */
class ListWidgetAlterGlobalForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'list_widget_alter_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $config = $this->config('list_widget_alter.settings');

    $form['removed_option_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Removed option help text'),
      '#description' => '<p>' . $this->t('If an option is removed using this widget, but was previously selected in a content element, this message will appear next to the label, indicating the user that the option is no longer available.') . '</p>',
      '#default_value' => $config->get('removed_option_message'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('list_widget_alter.settings')
      ->set('removed_option_message', $form_state->getValue('removed_option_message'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'list_widget_alter.settings',
    ];
  }

}
